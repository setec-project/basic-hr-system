/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentUtil;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;


/**
 *
 * @author XXXXX
 */
public class TextFieldUtil {
    
    public static final int USE_FLOAT_NUMBER = 0;
    public static final int NO_FLOAT_NUMBER = 1;
    
    public static void acceptNumberOnly(JTextComponent textField, int option) {
        
        if (option == NO_FLOAT_NUMBER) {
             textField.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyTyped(KeyEvent e) {
                      if (!Character.isDigit(e.getKeyChar()) && e.getKeyCode() != 8) { // 8 = back space
                           e.consume();
                      }
                  }
             });
        } else if (option == USE_FLOAT_NUMBER) {
            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyTyped(KeyEvent e) {
                    
                    int keyCode = (int)e.getKeyChar();
                    JTextComponent component = (JTextComponent)e.getComponent();
                    int textLength = component.getText().length();
                    if (keyCode == 46) {
                        if (textLength == 0 || component.getText().contains(".")) {
                            e.consume();
                        }
                    }
                    
                    if (!Character.isDigit(keyCode) && keyCode != 8 && keyCode != 46) { // 8 = back space, 46 = "." (dot)
                        e.consume();
                    }
                }
                
            });
        }
    }
    
    public static void limitTextLength(JTextComponent textField, int limit) {
        if (limit <= 0) return;
        
        textField.addKeyListener(new KeyAdapter() {
            
            @Override
            public void keyTyped(KeyEvent e) {

                JTextComponent component = (JTextComponent)e.getComponent();
                if (component.getText().length() >= limit) {
                    e.consume();
                }
            }
            
        });
    }
    
}
