/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentUtil;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author XXXXX
 */
public class TableUtil {
    public static void removeAllRows(JTable table) {
        DefaultTableModel tableModel = (DefaultTableModel)table.getModel();
        while (tableModel.getRowCount() > 0) {
            tableModel.removeRow(0);
        }
    }
    
    public static void setCellTextAlignment(JTable table, int alignment)
    {
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(alignment);

        TableModel tableModel = table.getModel();

        table.setDefaultRenderer(Object.class, rightRenderer);
    }
    
    public static void hideColumns(String[] columnsName, JTable table) {
        for (String columnName : columnsName) {
            table.removeColumn(table.getColumn(columnName));
        }
    }
    
}
