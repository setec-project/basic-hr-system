/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentUtil;

import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

/**
 *
 * @author XXXXX
 */
public class ComboBoxUtil {
    
    public static void loadDataToComboBox(JComboBox comboBox, List data) {
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(data.toArray());
        
        comboBox.setModel(comboBoxModel);
    }
    
    public static void loadDataToComboBox(JComboBox comboBox, List data, List appendData) {
        List<Object> result = new ArrayList<>();
        
        for (Object object : appendData) {
            result.add(object);
        }
        
        for (Object object : data) {
            result.add(object);
        }
        
        DefaultComboBoxModel comboBoxModel = new DefaultComboBoxModel(result.toArray());
        comboBox.setModel(comboBoxModel);
    }
    
}
