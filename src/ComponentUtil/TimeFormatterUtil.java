/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author XXXXX
 */
public class TimeFormatterUtil {
    
    public static String format(LocalDateTime localDateTime) {
        return localDateTime.format(DateTimeFormatter.ofPattern("MMM-dd-yyyy"));
    }
    
}
