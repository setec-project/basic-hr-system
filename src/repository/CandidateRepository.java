/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import java.util.Dictionary;
import java.util.List;
import model.Candidate;

/**
 *
 * @author XXXXX
 */
public interface CandidateRepository {
    int insert(Candidate candidate) throws SQLException;
    List<Dictionary<String, Object>> getNotInterviewedCandidate() throws SQLException;
    List<Dictionary<String, Object>> pendingInterviewCandidate() throws SQLException;
    List<Dictionary<String, Object>> getPassCandidate() throws SQLException;
    List<Dictionary<String, Object>> getFailedCandidate() throws SQLException;
    int getTotalInterviewTimeOfCandidate(int candidateId) throws SQLException;
    int getNumberInterviewOfCandidate(int candidateId) throws SQLException;
    int upgradeCandidate(int employeeId, int candidateId) throws SQLException;
}
