/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import model.User;

/**
 *
 * @author XXXXX
 */
public interface UserRepository {
    User login(User user) throws SQLException;
    Integer insert(User user) throws SQLException;
}
