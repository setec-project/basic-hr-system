/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import model.Interview;

/**
 *
 * @author XXXXX
 */
public interface InterviewRepository {
    
    int insert(Interview interview) throws SQLException;
    int updateIntervewStatus(int candidateId, int interviewType, boolean isPass) throws SQLException;
    
}
