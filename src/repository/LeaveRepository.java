/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import model.Leave;

/**
 *
 * @author XXXXX
 */
public interface LeaveRepository {
    
    Integer insert(Leave leave) throws SQLException;
    
    List<Map<String, Object>> getLeavePending() throws SQLException;
    
    void updateLeaveStatus(int leaveId, boolean status) throws SQLException;
    
    List<Map<String, Object>> getLeaveApproved() throws SQLException;
    
    List<Map<String, Object>> getLeaveRefused() throws SQLException;
    
}
