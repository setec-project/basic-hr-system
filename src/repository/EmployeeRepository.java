/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import java.util.Dictionary;
import java.util.List;
import java.util.Map;
import model.Employee;

/**
 *
 * @author XXXXX
 */
public interface EmployeeRepository {
    
    List<Employee> getAll() throws SQLException;
    
    List<Employee> findByName(String name) throws SQLException;
    
    int insert(Employee employee) throws SQLException;
    
    Employee getEmployeeByUserId(int userId) throws SQLException;
    
    List<Map<String, Object>> getAllEmployeeDetails() throws SQLException;
    
}
