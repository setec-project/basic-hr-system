/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import java.util.List;
import model.Bank;

/**
 *
 * @author XXXXX
 */
public interface BankRepository {
    
    List<Bank> getAll() throws SQLException;
    
}
