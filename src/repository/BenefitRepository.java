/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.SQLException;
import java.util.List;
import model.Benefit;

/**
 *
 * @author XXXXX
 */
public interface BenefitRepository {
    
    List<Benefit> getAll() throws SQLException;
    
    int insertBenefitOfEmployee(int benefitId, int employeeId) throws SQLException;
    
}
