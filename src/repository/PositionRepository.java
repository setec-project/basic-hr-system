/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.util.List;
import java.sql.SQLException;
import model.Position;

/**
 *
 * @author XXXXX
 */
public interface PositionRepository {
    
    List<Position> getAllPositions() throws Exception;
    
    Position getPositionById(int positionId) throws Exception;
    
    Position getPositionByEmployeeId(int employeeId) throws SQLException;
    
    Integer insert(Position position) throws SQLException;
    
}
