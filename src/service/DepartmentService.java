/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Department;
import repository.DepartmentRepository;

/**
 *
 * @author XXXXX
 */
public class DepartmentService implements DepartmentRepository {
    
    private DbConnection dbConnection;
    
    private static DepartmentService instance;
    
    private DepartmentService() {
        dbConnection = DbConnection.create();
    }
    
    public static DepartmentService create() {
        if (instance == null)
            instance = new DepartmentService();
        return instance;
    }

    @Override
    public List<Department> getAll() throws SQLException {
        String sql = "SELECT department_id, department_name FROM hr_departments";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Department> departments = new ArrayList<>();
        while (resultSet.next()) {
            Department department = new Department();
            department.setDepartmentId(resultSet.getInt("department_id"));
            department.setDepartmentName(resultSet.getString("department_name"));
            
            departments.add(department);
        }
        
        return departments;
    }

    @Override
    public Integer insert(Department department) throws SQLException {
        
        String sql = "INSERT INTO hr_departments (department_name) VALUES (?)";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setString(1, department.getDepartmentName());
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        resultSet.next();
        
        int rowId = resultSet.getInt("department_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
        
    }
    
}
