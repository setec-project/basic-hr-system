/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Benefit;
import repository.BenefitRepository;

/**
 *
 * @author XXXXX
 */
public class BenefitService implements BenefitRepository {
    
    private DbConnection dbConnection;
    private static BenefitService instance;
    
    private BenefitService() {
        dbConnection = DbConnection.create();
    }
    
    public static BenefitService create() {
        if (instance == null)
            instance = new BenefitService();
        return instance;
    }

    @Override
    public List<Benefit> getAll() throws SQLException {
        String sql = "SELECT benefit_id, benefit_name, pay_rate FROM hr_benefits;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Benefit> benefits = new ArrayList<>();
        while (resultSet.next()) {
            Benefit benefit = new Benefit();
            benefit.setBenefitId(resultSet.getInt("benefit_id"));
            benefit.setBenefitName(resultSet.getString("benefit_name"));
            benefit.setPayrate(resultSet.getDouble("pay_rate"));
            
            benefits.add(benefit);
        }
        
        return benefits;
    }

    @Override
    public int insertBenefitOfEmployee(int benefitId, int employeeId) throws SQLException {
        String sql = "INSERT INTO hr_benefit_employee (employee_id, benefit_id) VALUES (?, ?);";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setInt(1, employeeId);
        statement.setInt(2, benefitId);
        
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        int rowId = -1;
        if (resultSet.next())
            rowId = resultSet.getInt("benefit_employee_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }
    
    
}
