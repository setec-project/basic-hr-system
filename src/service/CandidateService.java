/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import ComponentUtil.TimeFormatterUtil;
import java.math.BigDecimal;
import model.Candidate;
import java.sql.Connection;
import java.sql.SQLException;
import repository.CandidateRepository;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

/**
 *
 * @author XXXXX
 */
public class CandidateService implements CandidateRepository {

    private DbConnection dbConnection;

    private static CandidateService instance;

    private CandidateService() {
        dbConnection = DbConnection.create();
    }

    public static CandidateService create() {
        if (instance == null) {
            instance = new CandidateService();
        }
        return instance;
    }

    @Override
    public int insert(Candidate candidate) throws SQLException {
        Connection connection = dbConnection.createConnection();
        String sql = "INSERT INTO hr_candidates (candidate_name, email, tel, address, status, dob, gender, apply_for, number_of_interview, expect_salary)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, candidate.getCandidateName());
        statement.setString(2, candidate.getEmail());
        statement.setString(3, candidate.getTel());
        statement.setString(4, candidate.getAddress());
        statement.setString(5, candidate.getStatus());
        statement.setTimestamp(6, candidate.getDob());
        statement.setString(7, candidate.getGender());
        statement.setInt(8, candidate.getPosition());
        statement.setInt(9, candidate.getNumberOfInterview());
        statement.setBigDecimal(10, new BigDecimal(candidate.getExpectSalary()));

        statement.executeUpdate();
        ResultSet primaryKey = statement.getGeneratedKeys();

        primaryKey.next();
        int id = primaryKey.getInt("id");

        primaryKey.close();
        statement.close();
        connection.close();

        return id;
    }

    @Override
    public List<Dictionary<String, Object>> getNotInterviewedCandidate() throws SQLException {
        String sql = "SELECT candidate_id, candidate_name, dob, gender, email, address, apply_for, status, position, expect_salary, result, total_interviewed, number_of_interview"
                + " FROM v_number_of_interviewed_candidate WHERE total_interviewed = 0;";
        Connection connection = dbConnection.createConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        List<Dictionary<String, Object>> notInterviewdCandidates = new ArrayList<Dictionary<String, Object>>();
        while (resultSet.next()) {
            Dictionary<String, Object> row = new Hashtable<>();
            row.put("candidate_id", resultSet.getInt("candidate_id"));
            row.put("apply_for", resultSet.getInt("apply_for"));
            row.put("candidate_name", resultSet.getString("candidate_name"));
            row.put("dob", TimeFormatterUtil.format(resultSet.getTimestamp("dob").toLocalDateTime()));
            row.put("gender", resultSet.getString("gender"));
            row.put("email", resultSet.getString("email"));
            row.put("address", resultSet.getString("address"));
            row.put("position", resultSet.getString("position"));
            row.put("status", resultSet.getString("status"));
            row.put("expect_salary", resultSet.getDouble("expect_salary"));
            row.put("result", resultSet.getInt("result"));
            row.put("total_interviewed", resultSet.getInt("total_interviewed"));
            row.put("number_of_interview", resultSet.getInt("number_of_interview"));
            notInterviewdCandidates.add(row);
        }

        resultSet.close();
        statement.close();
        connection.close();

        return notInterviewdCandidates;
    }

    @Override
    public List<Dictionary<String, Object>> pendingInterviewCandidate() throws SQLException {
        String sql = "SELECT candidate_id, candidate_name, dob, gender, email, address, apply_for, status, position, expect_salary, result, total_interviewed, number_of_interview, pending"
                + " FROM v_number_of_interviewed_candidate"
                + " WHERE pending IS NULL AND total_interviewed > 0";
        Connection connection = dbConnection.createConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        List<Dictionary<String, Object>> pendingInterviewCandidates = new ArrayList<Dictionary<String, Object>>();
        while (resultSet.next()) {
            Dictionary<String, Object> row = new Hashtable<>();
            row.put("candidate_id", resultSet.getInt("candidate_id"));
            row.put("apply_for", resultSet.getInt("apply_for"));
            row.put("candidate_name", resultSet.getString("candidate_name"));
            row.put("dob", TimeFormatterUtil.format(resultSet.getTimestamp("dob").toLocalDateTime()));
            row.put("gender", resultSet.getString("gender"));
            row.put("email", resultSet.getString("email"));
            row.put("address", resultSet.getString("address"));
            row.put("position", resultSet.getString("position"));
            row.put("status", resultSet.getString("status"));
            row.put("expect_salary", resultSet.getDouble("expect_salary"));
            row.put("result", resultSet.getInt("result"));
            row.put("total_interviewed", resultSet.getInt("total_interviewed"));
            row.put("number_of_interview", resultSet.getInt("number_of_interview"));
            pendingInterviewCandidates.add(row);
        }

        resultSet.close();
        statement.close();
        connection.close();

        return pendingInterviewCandidates;
    }

    @Override
    public int getTotalInterviewTimeOfCandidate(int candidateId) throws SQLException {
        String sql = "SELECT total_interviewed"
                + " FROM v_number_of_interviewed_candidate WHERE candidate_id = ?;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, candidateId);
        ResultSet resultSet = statement.executeQuery();

        int totalInterviewTime = 0;
        if (resultSet.next()) {
            totalInterviewTime = resultSet.getInt("total_interviewed");
        }

        resultSet.close();
        statement.close();
        connection.close();

        return totalInterviewTime;
    }

    @Override
    public int getNumberInterviewOfCandidate(int candidateId) throws SQLException {
        String sql = "SELECT number_of_interview"
                + " FROM v_number_of_interviewed_candidate WHERE candidate_id = ?;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, candidateId);
        ResultSet resultSet = statement.executeQuery();

        int totalInterviewTime = 0;
        if (resultSet.next()) {
            totalInterviewTime = resultSet.getInt("number_of_interview");
        }

        resultSet.close();
        statement.close();
        connection.close();

        return totalInterviewTime;
    }

    @Override
    public List<Dictionary<String, Object>> getPassCandidate() throws SQLException {
        String sql = "SELECT candidate_id, candidate_name, dob, gender, email, address, apply_for, status, position, expect_salary, result, total_interviewed, number_of_interview"
                + " FROM v_number_of_interviewed_candidate"
                + " WHERE result = number_of_interview;";
        Connection connection = dbConnection.createConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        List<Dictionary<String, Object>> pendingInterviewCandidates = new ArrayList<Dictionary<String, Object>>();
        while (resultSet.next()) {
            Dictionary<String, Object> row = new Hashtable<>();
            row.put("candidate_id", resultSet.getInt("candidate_id"));
            row.put("apply_for", resultSet.getInt("apply_for"));
            row.put("candidate_name", resultSet.getString("candidate_name"));
            row.put("dob", resultSet.getTimestamp("dob"));
            row.put("gender", resultSet.getString("gender"));
            row.put("email", resultSet.getString("email"));
            row.put("address", resultSet.getString("address"));
            row.put("position", resultSet.getString("position"));
            row.put("status", resultSet.getString("status"));
            row.put("expect_salary", resultSet.getDouble("expect_salary"));
            row.put("result", resultSet.getInt("result"));
            row.put("total_interviewed", resultSet.getInt("total_interviewed"));
            row.put("number_of_interview", resultSet.getInt("number_of_interview"));
            pendingInterviewCandidates.add(row);
        }

        resultSet.close();
        statement.close();
        connection.close();

        return pendingInterviewCandidates;
    }

    @Override
    public List<Dictionary<String, Object>> getFailedCandidate() throws SQLException {
        String sql = "SELECT candidate_id, candidate_name, dob, gender, email, address, tel, apply_for, status, position, expect_salary, result, total_interviewed, number_of_interview, pending"
                + " FROM v_number_of_interviewed_candidate"
                + " WHERE pending = 0 AND total_interviewed > 0 AND result < number_of_interview";
        Connection connection = dbConnection.createConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        List<Dictionary<String, Object>> pendingInterviewCandidates = new ArrayList<Dictionary<String, Object>>();
        while (resultSet.next()) {
            Dictionary<String, Object> row = new Hashtable<>();
            row.put("candidate_id", resultSet.getInt("candidate_id"));
            row.put("apply_for", resultSet.getInt("apply_for"));
            row.put("candidate_name", resultSet.getString("candidate_name"));
            row.put("dob", TimeFormatterUtil.format(resultSet.getTimestamp("dob").toLocalDateTime()));
            row.put("gender", resultSet.getString("gender"));
            row.put("email", resultSet.getString("email"));
            row.put("address", resultSet.getString("address"));
            row.put("position", resultSet.getString("position"));
            row.put("status", resultSet.getString("status"));
            row.put("expect_salary", resultSet.getDouble("expect_salary"));
            row.put("result", resultSet.getInt("result"));
            row.put("total_interviewed", resultSet.getInt("total_interviewed"));
            row.put("number_of_interview", resultSet.getInt("number_of_interview"));
            row.put("tel", resultSet.getString("tel"));
            pendingInterviewCandidates.add(row);
        }

        resultSet.close();
        statement.close();
        connection.close();

        return pendingInterviewCandidates;
    }

    @Override
    public int upgradeCandidate(int employeeId, int candidateId) throws SQLException {
        String sql = "INSERT INTO hr_upgrade_candidate (employee_id, candidate_id) VALUES (?, ?);";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, employeeId);
        statement.setInt(2, candidateId);
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        
        int rowId = -1;
        if (resultSet.next())
            rowId = resultSet.getInt("candidate_upgrade_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }


}
