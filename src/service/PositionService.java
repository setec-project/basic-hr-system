/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Position;
import repository.PositionRepository;

/**
 *
 * @author XXXXX
 */
public class PositionService implements PositionRepository {

    private DbConnection dbConnection;
    
    private static PositionService instance;
    
    private PositionService() {
        dbConnection = DbConnection.create();
    }
    
    public static PositionService create() {
        if (instance == null)
            instance = new PositionService();
        return instance;
    }
    
    @Override
    public List<Position> getAllPositions() throws SQLException {
        String sql = "SELECT position_id, position FROM hr_positions";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Position> positions = new ArrayList<>();
        while (resultSet.next()) {
            Position position = new Position();
            position.setPositionId(resultSet.getInt("position_id"));
            position.setPosition(resultSet.getString("position"));
            positions.add(position);
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return positions;
    }

    @Override
    public Position getPositionById(int positionId) throws SQLException {
        String sql = "SELECT position_id, position FROM hr_positions"
                + " WHERE position_id = ?;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, positionId);
        
        ResultSet resultSet = statement.executeQuery();
        Position position = null;
        
        if (resultSet.next()) {
            position = new Position();
            position.setPositionId(resultSet.getInt("position_id"));
            position.setPosition(resultSet.getString("position"));
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return position;
    }
    
    @Override
    public Position getPositionByEmployeeId(int employeeId) throws SQLException {
        String sql = "SELECT position_id, position FROM v_employee_position"
                + " WHERE employee_id = ?;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, employeeId);
        
        ResultSet resultSet = statement.executeQuery();
        Position position = null;
        
        if (resultSet.next()) {
            position = new Position();
            position.setPositionId(resultSet.getInt("position_id"));
            position.setPosition(resultSet.getString("position"));
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return position;
    }

    @Override
    public Integer insert(Position position) throws SQLException {
        
        String sql = "INSERT INTO hr_positions (position) VALUES (?)";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setString(1, position.getPosition());
        
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        resultSet.next();
        int rowId = resultSet.getInt("position_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
        
    }
    
}
