/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import model.Employee;
import repository.EmployeeRepository;

/**
 *
 * @author XXXXX
 */
public class EmployeeService implements EmployeeRepository{
    
    private DbConnection dbConnection;
    private static EmployeeService instance;
    
    private EmployeeService() {
        dbConnection = DbConnection.create();
    }
    
    public static EmployeeService create() {
        if (instance == null)
            instance = new EmployeeService();
        return instance;
    }

    @Override
    public List<Employee> getAll() throws SQLException {
        Connection connection = dbConnection.createConnection();
        String sql = "SELECT employee_id, first_name, last_name, email, address, status, dob, gender, position, bank_account_number, base_salary, ot_in_one_hour, bank_id, department_id"
                + " FROM hr_employees;";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Employee> allEmployees = new ArrayList<>();
        while (resultSet.next()) {
            Employee employee = new Employee();
            employee.setEmployeeId(resultSet.getInt("employee_id"));
            employee.setFirstName(resultSet.getString("first_name"));
            employee.setLastName(resultSet.getString("last_name"));
            employee.setEmail(resultSet.getString("email"));
            employee.setAddress(resultSet.getString("address"));
            employee.setStatus(resultSet.getString("status"));
            employee.setDob(resultSet.getTimestamp("dob"));
            employee.setGender(resultSet.getString("gender"));
            employee.setPositionId(resultSet.getInt("position_id"));
            employee.setBankAccountNumber(resultSet.getString("bank_account_number"));
            employee.setBaseSalary(resultSet.getDouble("base_salary"));
            employee.setOtInOneHour(resultSet.getDouble("ot_in_one_hour"));
            employee.setBankId(resultSet.getInt("bank_id"));
            employee.setDepartmentId(resultSet.getInt("department_id"));
            allEmployees.add(employee);
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return allEmployees;
    }

    @Override
    public List<Employee> findByName(String name) throws SQLException {
        Connection connection = dbConnection.createConnection();
        String sql = "SELECT employee_id, first_name, last_name, email, address, status, dob, gender, position_id, bank_account_number, base_salary, ot_in_one_hour, bank_id, department_id"
                + " FROM hr_employees"
                + " WHERE first_name LIKE ? OR last_name LIKE ?;";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, "%" + name + "%");
        statement.setString(2, "%" + name + "%");
        ResultSet resultSet = statement.executeQuery();
        
        List<Employee> allEmployees = new ArrayList<>();
        while (resultSet.next()) {
            Employee employee = new Employee();
            employee.setEmployeeId(resultSet.getInt("employee_id"));
            employee.setFirstName(resultSet.getString("first_name"));
            employee.setLastName(resultSet.getString("last_name"));
            employee.setEmail(resultSet.getString("email"));
            employee.setAddress(resultSet.getString("address"));
            employee.setStatus(resultSet.getString("status"));
            employee.setDob(resultSet.getTimestamp("dob"));
            employee.setGender(resultSet.getString("gender"));
            employee.setPositionId(resultSet.getInt("position_id"));
            employee.setBankAccountNumber(resultSet.getString("bank_account_number"));
            employee.setBaseSalary(resultSet.getDouble("base_salary"));
            employee.setOtInOneHour(resultSet.getDouble("ot_in_one_hour"));
            employee.setBankId(resultSet.getInt("bank_id"));
            employee.setDepartmentId(resultSet.getInt("department_id"));
            allEmployees.add(employee);
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return allEmployees;
    }

    @Override
    public int insert(Employee employee) throws SQLException {
        String sql = "INSERT INTO hr_employees (first_name, last_name, email, address, status, dob, gender, position_id, bank_account_number, base_salary, ot_in_one_hour, bank_id, department_id)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setString(1, employee.getFirstName());
        statement.setString(2, employee.getLastName());
        statement.setString(3, employee.getEmail());
        statement.setString(4, employee.getAddress());
        statement.setString(5, employee.getStatus());
        statement.setTimestamp(6, employee.getDob());
        statement.setString(7, employee.getGender());
        statement.setInt(8, employee.getPositionId());
        statement.setString(9, employee.getBankAccountNumber());
        statement.setBigDecimal(10, BigDecimal.valueOf(employee.getBaseSalary()));
        statement.setBigDecimal(11, BigDecimal.valueOf(employee.getOtInOneHour()));
        statement.setObject(12, employee.getBankId(), Types.INTEGER);
        statement.setObject(13, employee.getDepartmentId(), Types.INTEGER);
        
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        
        int rowId = -1;
        if (resultSet.next())
            rowId = resultSet.getInt("employee_id");
        
        return rowId;
    }

    @Override
    public Employee getEmployeeByUserId(int userId) throws SQLException {
        Connection connection = dbConnection.createConnection();
        String sql = "SELECT employee_id, first_name, last_name, email, address, status, dob, gender, position_id, bank_account_number, base_salary, ot_in_one_hour, bank_id, department_id"
                + " FROM v_employee_user"
                + " WHERE user_id = ?;";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, userId);
        ResultSet resultSet = statement.executeQuery();
        
        Employee employee = null;
        if (resultSet.next()) {
            employee = new Employee();
            employee.setEmployeeId(resultSet.getInt("employee_id"));
            employee.setFirstName(resultSet.getString("first_name"));
            employee.setLastName(resultSet.getString("last_name"));
            employee.setEmail(resultSet.getString("email"));
            employee.setAddress(resultSet.getString("address"));
            employee.setStatus(resultSet.getString("status"));
            employee.setDob(resultSet.getTimestamp("dob"));
            employee.setGender(resultSet.getString("gender"));
            employee.setPositionId(resultSet.getInt("position_id"));
            employee.setBankAccountNumber(resultSet.getString("bank_account_number"));
            employee.setBaseSalary(resultSet.getDouble("base_salary"));
            employee.setOtInOneHour(resultSet.getDouble("ot_in_one_hour"));
            employee.setBankId(resultSet.getInt("bank_id"));
            employee.setDepartmentId(resultSet.getInt("department_id"));
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return employee;
    }

    @Override
    public List<Map<String, Object>> getAllEmployeeDetails() throws SQLException {
        
        String sql = "SELECT * FROM v_employee_details";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Map<String, Object>> employeeDetails = new ArrayList<>();
        while (resultSet.next()) {
            Map<String, Object> employeeDetail = new HashMap<>();
            employeeDetail.put("employee_id", resultSet.getInt("employee_id"));
            employeeDetail.put("first_name", resultSet.getString("first_name"));
            employeeDetail.put("last_name", resultSet.getString("last_name"));
            employeeDetail.put("email", resultSet.getString("email"));
            employeeDetail.put("address", resultSet.getString("address"));
            employeeDetail.put("status", resultSet.getString("status"));
            employeeDetail.put("dob", resultSet.getTimestamp("dob"));
            employeeDetail.put("gender", resultSet.getString("gender"));
            employeeDetail.put("position_id", resultSet.getInt("position_id"));
            employeeDetail.put("position", resultSet.getString("position"));
            employeeDetail.put("bank_account_number", resultSet.getString("bank_account_number"));
            employeeDetail.put("bank_id", resultSet.getInt("bank_id"));
            employeeDetail.put("bank_name", resultSet.getString("bank_name"));
            employeeDetail.put("base_salary", resultSet.getDouble("base_salary"));
            employeeDetail.put("ot_in_one_hour", resultSet.getDouble("ot_in_one_hour"));
            employeeDetail.put("department_id", resultSet.getInt("department_id"));
            employeeDetail.put("department_name", resultSet.getString("department_name"));
            
            employeeDetails.add(employeeDetail);
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return employeeDetails;
        
    }
    
}
