/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Interview;
import org.mariadb.jdbc.internal.util.dao.PrepareResult;
import repository.InterviewRepository;

/**
 *
 * @author XXXXX
 */
public class InterviewService implements InterviewRepository {

    private DbConnection dbConnection;
    
    private static InterviewService instance;
    
    private InterviewService() {
        dbConnection = DbConnection.create();
    }
    
    public static InterviewService create() {
        if (instance == null)
            instance = new InterviewService();
        return instance;
    }
    
    @Override
    public int insert(Interview interview) throws SQLException {
        String sql = "INSERT INTO hr_interview (interview_date, interview_type, candidate_id, start_time, end_time) VALUES (?, ?, ?, ?, ?)";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setTimestamp(1, interview.getInterviewDate());
        statement.setInt(2, interview.getInterviewType());
        statement.setInt(3, interview.getCandidateId());
        statement.setString(4, interview.getStartTime());
        statement.setString(5, interview.getEndTime());
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        
        int rowId = -1;
        if (resultSet.next()) {
            rowId = resultSet.getInt("interview_id");
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }

    @Override
    public int updateIntervewStatus(int candidateId, int interviewType, boolean isPass) throws SQLException {
        String sql = "UPDATE hr_interview SET is_passed = ? WHERE candidate_id = ? AND interview_type = ?;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setBoolean(1, isPass);
        statement.setInt(2, candidateId);
        statement.setInt(3, interviewType);
        statement.executeUpdate();
        
        ResultSet resultSet = statement.getGeneratedKeys();
        int rowId = -1;
        if (resultSet.next())
            rowId = resultSet.getInt("interview_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }
    
}
