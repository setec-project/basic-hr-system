/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import org.mariadb.jdbc.Driver;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author XXXXX
 */
public class DbConnection {
    private static final String CONNECTION_URL = "jdbc:mariadb://localhost:3306/hr";
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    private static DbConnection instance;
    
    private DbConnection() {}
    
    public static DbConnection create() {
        if (instance == null)
            instance = new DbConnection();
        return instance;
    }
    
    public Connection createConnection() throws SQLException {
        Driver driver = new Driver();
        Properties properties = new Properties();
        properties.setProperty("user", USER);
        properties.setProperty("password", PASSWORD);
        return driver.connect(CONNECTION_URL, properties);
    }
    
}
