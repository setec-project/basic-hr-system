/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Bank;
import repository.BankRepository;

/**
 *
 * @author XXXXX
 */
public class BankService implements BankRepository {
    
    private DbConnection dbConnection;
    private static BankService instance;
    
    private BankService() {
        dbConnection = DbConnection.create();
    }
    
    public static BankService create() {
        if (instance == null)
            instance = new BankService();
        
        return instance;
    }

    @Override
    public List<Bank> getAll() throws SQLException {
        String sql = "SELECT bank_id, bank_name FROM hr_banks;";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Bank> banks = new ArrayList<>();
        while (resultSet.next()) {
            Bank bank = new Bank();
            bank.setBankId(resultSet.getInt("bank_id"));
            bank.setBankName(resultSet.getString("bank_name"));
            
            banks.add(bank);
        }
        
        return banks;
    }
    
    
    
}
