/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 *
 * @author XXXXX
 */
public class AlertService {
    
    private static AlertService instance;
    
    private AlertService() {}
    
    public static AlertService create() {
        if (instance == null)
            instance = new AlertService();
        return instance;
    }
    
    public void alertError(Component parent) {
        JOptionPane.showMessageDialog(parent, "Unexpect error while working with database.", "", JOptionPane.ERROR_MESSAGE);
    }
    
}
