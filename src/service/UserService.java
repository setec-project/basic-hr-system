/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import model.User;
import repository.UserRepository;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author XXXXX
 */
public class UserService implements UserRepository{

    private DbConnection dbConnection;
    
    private static UserService instance;
    
    private UserService() {
        this.dbConnection = DbConnection.create();
    }
    
    public static UserService create() {
        if (instance == null)
            instance = new UserService();
        
        return instance;
    }
    
    @Override
    public User login(User user) throws SQLException {
        
        Connection connection = dbConnection.createConnection();
        String sql = "SELECT user_id, username, password, role, employee_id FROM hr_users WHERE username = ? AND password = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, user.getUserName());
        statement.setString(2, user.getPassword());
        
        ResultSet resultSet = statement.executeQuery();
        User authenticationUser = null;
        if (resultSet.next()) {
            authenticationUser = new User();
            authenticationUser.setId(resultSet.getInt("user_id"));
            authenticationUser.setUserName(resultSet.getString("username"));
            authenticationUser.setPassword(resultSet.getString("password"));
            authenticationUser.setRole(resultSet.getString("role"));
            authenticationUser.setEmployeeId(resultSet.getInt("employee_id"));
        }

        resultSet.close();
        statement.close();
        connection.close();

        return authenticationUser;
    }

    @Override
    public Integer insert(User user) throws SQLException {
        
        String sql = "INSERT INTO hr_users (employee_id, username, password, role) VALUES (?, ?, ?, ?);";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setInt(1, user.getEmployeeId());
        statement.setString(2, user.getUserName());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole());
        
        statement.executeUpdate();
        
        ResultSet resultSet = statement.getGeneratedKeys();
        resultSet.next();
        
        int rowId = resultSet.getByte("user_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
        
    }
    
}
