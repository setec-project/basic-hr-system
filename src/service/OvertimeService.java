/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import model.OverTime;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import repository.OverTimeRepository;

/**
 *
 * @author XXXXX
 */
public class OvertimeService implements OverTimeRepository {
    
    private DbConnection dbConnection;
    private static OvertimeService instance;
    
    private OvertimeService() {
        dbConnection = DbConnection.create();
    }
    
    public static OvertimeService create() {
        if (instance == null)
            instance = new OvertimeService();
        return instance;
    }

    @Override
    public Integer insert(OverTime ot) throws SQLException {
        String sql = "INSERT INTO hr_over_time (employee_id, ot_date, start_time, end_time, hours, description) VALUES (?, ?, ?, ?, ?, ?)";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, ot.getEmployeeId());
        statement.setTimestamp(2, ot.getOtDate());
        statement.setString(3, ot.getStartTime());
        statement.setString(4, ot.getEndTime());
        statement.setDouble(5, ot.getHour());
        statement.setString(6, ot.getDescription());
        
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        
        resultSet.next();
        int rowId = resultSet.getInt("over_time_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }
    
}
