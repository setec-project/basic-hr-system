/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import ComponentUtil.TimeFormatterUtil;
import com.mysql.cj.util.TimeUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import model.Leave;
import repository.LeaveRepository;

/**
 *
 * @author XXXXX
 */
public class LeaveService implements LeaveRepository {
    
    private DbConnection dbConnection;
    
    private static LeaveService instance;
    
    private LeaveService() {
        dbConnection = DbConnection.create();
    }
    
    public static LeaveService create() {
        if (instance == null)
            instance = new LeaveService();
        
        return instance;
    }

    @Override
    public Integer insert(Leave leave) throws SQLException {
        String sql = "INSERT INTO hr_leave (employee_id, request_date, start_date, end_date, days, reason, status, description) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        
        statement.setInt(1, leave.getEmployeeId());
        statement.setTimestamp(2, leave.getRequestDate());
        statement.setTimestamp(3, leave.getStartDate());
        statement.setTimestamp(4, leave.getEndDate());
        statement.setDouble(5, leave.getDays());
        statement.setString(6, leave.getReason());
        statement.setObject(7, leave.getStatus(), Types.BOOLEAN);
        statement.setString(8, leave.getDescription());
        
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        resultSet.next();
        int rowId = resultSet.getInt("leave_id");
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return rowId;
    }

    @Override
    public List<Map<String, Object>> getLeavePending() throws SQLException {
         
        String sql = "SELECT leave_id, first_name, last_name, gender, position, department_name, request_date, start_date, end_date, days, status, reason, description"
                + " FROM v_leave_detail"
                + " WHERE status IS NULL";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Map<String, Object>> pendingLeaves = new ArrayList<>();
        while (resultSet.next()) {
            
            Map<String, Object> pendingLeave = new HashMap<>();
            pendingLeave.put("leave_id", resultSet.getInt("leave_id"));
            pendingLeave.put("first_name", resultSet.getString("first_name"));
            pendingLeave.put("last_name", resultSet.getString("last_name"));
            pendingLeave.put("gender", resultSet.getString("gender"));
            pendingLeave.put("position", resultSet.getString("position"));
            pendingLeave.put("department_name", resultSet.getString("department_name"));
            pendingLeave.put("request_date", TimeFormatterUtil.format(resultSet.getTimestamp("request_date").toLocalDateTime()));
            pendingLeave.put("start_date", TimeFormatterUtil.format(resultSet.getTimestamp("start_date").toLocalDateTime()));
            pendingLeave.put("end_date", TimeFormatterUtil.format(resultSet.getTimestamp("end_date").toLocalDateTime()));
            pendingLeave.put("days", resultSet.getDouble("days"));
            pendingLeave.put("status", resultSet.getString("status"));
            pendingLeave.put("reason", resultSet.getString("reason"));
            pendingLeave.put("description", resultSet.getString("description"));
            
            pendingLeaves.add(pendingLeave);
            
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return pendingLeaves;
        
        
    }

    @Override
    public void updateLeaveStatus(int leaveId, boolean status) throws SQLException {
        
        String sql = "UPDATE hr_leave SET status = ? WHERE leave_id = ?";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        
        statement.setBoolean(1, status);
        statement.setInt(2, leaveId);
        
        statement.executeUpdate();
        
        statement.close();
        connection.close();
        
    }

    @Override
    public List<Map<String, Object>> getLeaveApproved() throws SQLException {
        
        String sql = "SELECT leave_id, first_name, last_name, gender, position, department_name, request_date, start_date, end_date, days, status, reason, description"
                + " FROM v_leave_detail"
                + " WHERE status = true";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Map<String, Object>> approvedLeaves = new ArrayList<>();
        while (resultSet.next()) {
            
            Map<String, Object> pendingLeave = new HashMap<>();
            pendingLeave.put("leave_id", resultSet.getInt("leave_id"));
            pendingLeave.put("first_name", resultSet.getString("first_name"));
            pendingLeave.put("last_name", resultSet.getString("last_name"));
            pendingLeave.put("gender", resultSet.getString("gender"));
            pendingLeave.put("position", resultSet.getString("position"));
            pendingLeave.put("department_name", resultSet.getString("department_name"));
            pendingLeave.put("request_date", TimeFormatterUtil.format(resultSet.getTimestamp("request_date").toLocalDateTime()));
            pendingLeave.put("start_date", TimeFormatterUtil.format(resultSet.getTimestamp("start_date").toLocalDateTime()));
            pendingLeave.put("end_date", TimeFormatterUtil.format(resultSet.getTimestamp("end_date").toLocalDateTime()));
            pendingLeave.put("days", resultSet.getDouble("days"));
            pendingLeave.put("status", resultSet.getString("status"));
            pendingLeave.put("reason", resultSet.getString("reason"));
            pendingLeave.put("description", resultSet.getString("description"));
            
            approvedLeaves.add(pendingLeave);
            
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return approvedLeaves;
        
    }

    @Override
    public List<Map<String, Object>> getLeaveRefused() throws SQLException {
        
        String sql = "SELECT leave_id, first_name, last_name, gender, position, department_name, request_date, start_date, end_date, days, status, reason, description"
                + " FROM v_leave_detail"
                + " WHERE status = false";
        Connection connection = dbConnection.createConnection();
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        
        List<Map<String, Object>> approvedLeaves = new ArrayList<>();
        while (resultSet.next()) {
            
            Map<String, Object> pendingLeave = new HashMap<>();
            pendingLeave.put("leave_id", resultSet.getInt("leave_id"));
            pendingLeave.put("first_name", resultSet.getString("first_name"));
            pendingLeave.put("last_name", resultSet.getString("last_name"));
            pendingLeave.put("gender", resultSet.getString("gender"));
            pendingLeave.put("position", resultSet.getString("position"));
            pendingLeave.put("department_name", resultSet.getString("department_name"));
            pendingLeave.put("request_date", TimeFormatterUtil.format(resultSet.getTimestamp("request_date").toLocalDateTime()));
            pendingLeave.put("start_date", TimeFormatterUtil.format(resultSet.getTimestamp("start_date").toLocalDateTime()));
            pendingLeave.put("end_date", TimeFormatterUtil.format(resultSet.getTimestamp("end_date").toLocalDateTime()));
            pendingLeave.put("days", resultSet.getDouble("days"));
            pendingLeave.put("status", resultSet.getString("status"));
            pendingLeave.put("reason", resultSet.getString("reason"));
            pendingLeave.put("description", resultSet.getString("description"));
            
            approvedLeaves.add(pendingLeave);
            
        }
        
        resultSet.close();
        statement.close();
        connection.close();
        
        return approvedLeaves;
        
    }


    
}
