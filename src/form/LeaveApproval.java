/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import ComponentUtil.TableUtil;
import ComponentUtil.TimeFormatterUtil;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.table.DefaultTableModel;
import repository.LeaveRepository;
import service.LeaveService;

/**
 *
 * @author XXXXX
 */
public class LeaveApproval extends javax.swing.JPanel {

    private LeaveRepository leaveService;
    private JPopupMenu popupMenu;
    private JFrame mainForm;
    
    private static final Font DEFAULT_FONT = new Font("Tahoma", Font.PLAIN, 12);
    
    public LeaveApproval(JFrame mainForm) {
        initComponents();
        reInit(mainForm);
    }
    
    private void reInit(JFrame mainForm) {
        //
        leaveService = LeaveService.create();
        //
        this.mainForm = mainForm;
        //
        JMenuItem popupMenuItemApprove = new JMenuItem("Approve");
        popupMenuItemApprove.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                
                menuItemApproveClickHandler(e);
                
            }
            
        });
        popupMenuItemApprove.setFont(DEFAULT_FONT);
        JMenuItem popupMenuItemRefuse = new JMenuItem("Refuse");
        popupMenuItemRefuse.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                menuItemRefuseClickHandler(e);
            }
            
        });
        popupMenuItemRefuse.setFont(DEFAULT_FONT);
        popupMenu = new JPopupMenu();
        popupMenu.add(popupMenuItemApprove);
        popupMenu.add(popupMenuItemRefuse);
        //
        TableUtil.hideColumns(new String[] { "Leave Id" }, tableLeave);
        comboBoxOptionsItemStateChanged(null);
    }
    
    private void loadPendingLeaveToTable() {
        
        DefaultTableModel tableModel = (DefaultTableModel) tableLeave.getModel();
        TableUtil.removeAllRows(tableLeave);
        try {
            List<Map<String, Object>> leavePendings = leaveService.getLeavePending();
            for (Map<String, Object> leavePending : leavePendings) {
                
                Object[] row = {
                    leavePending.get("leave_id"),
                    leavePending.get("last_name"),
                    leavePending.get("first_name"),
                    leavePending.get("gender"),
                    leavePending.get("department_name"),
                    leavePending.get("position"),
                    leavePending.get("request_date"),
                    leavePending.get("start_date"),
                    leavePending.get("end_date"),
                    leavePending.get("days"),
                    leavePending.get("status"),
                    leavePending.get("reason"),
                    leavePending.get("description")
                };
                tableModel.addRow(row);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(LeaveApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void loadApprovedLeaveToTable() {
        
        DefaultTableModel tableModel = (DefaultTableModel) tableLeave.getModel();
        TableUtil.removeAllRows(tableLeave);
        try {
            List<Map<String, Object>> leavePendings = leaveService.getLeaveApproved();
            for (Map<String, Object> leavePending : leavePendings) {
                
                Object[] row = {
                    leavePending.get("leave_id"),
                    leavePending.get("last_name"),
                    leavePending.get("first_name"),
                    leavePending.get("gender"),
                    leavePending.get("department_name"),
                    leavePending.get("position"),
                    leavePending.get("request_date"),
                    leavePending.get("start_date"),
                    leavePending.get("end_date"),
                    leavePending.get("days"),
                    leavePending.get("status"),
                    leavePending.get("reason"),
                    leavePending.get("description")
                };
                tableModel.addRow(row);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(LeaveApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void loadRefusedLeaveToTable() {
        
        DefaultTableModel tableModel = (DefaultTableModel) tableLeave.getModel();
        TableUtil.removeAllRows(tableLeave);
        try {
            List<Map<String, Object>> leavePendings = leaveService.getLeaveRefused();
            for (Map<String, Object> leavePending : leavePendings) {
                
                Object[] row = {
                    leavePending.get("leave_id"),
                    leavePending.get("last_name"),
                    leavePending.get("first_name"),
                    leavePending.get("gender"),
                    leavePending.get("department_name"),
                    leavePending.get("position"),
                    leavePending.get("request_date"),
                    leavePending.get("start_date"),
                    leavePending.get("end_date"),
                    leavePending.get("days"),
                    leavePending.get("status"),
                    leavePending.get("reason"),
                    leavePending.get("description")
                };
                tableModel.addRow(row);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(LeaveApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void menuItemApproveClickHandler(MouseEvent evt) {
        
        int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure to approve this request ?", "", JOptionPane.OK_CANCEL_OPTION);
        if (dialogResult != JOptionPane.OK_OPTION)
            return;
        
        try {
            int selectedRow = tableLeave.getSelectedRow();
            int leaveId = (int) tableLeave.getModel().getValueAt(selectedRow, 0);
            leaveService.updateLeaveStatus(leaveId, true);
            JOptionPane.showMessageDialog(this, "Approved.", "", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(LeaveApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void menuItemRefuseClickHandler(MouseEvent evt) {
        
        int dialogResult = JOptionPane.showConfirmDialog(this, "Are you sure to refuse this request ?", "", JOptionPane.OK_CANCEL_OPTION);
        if (dialogResult != JOptionPane.OK_OPTION)
            return;
        
        try {
            int selectedRow = tableLeave.getSelectedRow();
            int leaveId = (int) tableLeave.getModel().getValueAt(selectedRow, 0);
            leaveService.updateLeaveStatus(leaveId, false);
            JOptionPane.showMessageDialog(this, "Refused.", "", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            Logger.getLogger(LeaveApproval.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableLeave = new javax.swing.JTable();
        comboBoxOptions = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        tableLeave.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tableLeave.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Leave Id", "Last Name", "First Name", "Gender", "Department", "Position", "Request Date", "Start Date", "End Date", "Days", "Status", "Reason", "Description"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableLeave.setRowHeight(20);
        tableLeave.setRowMargin(3);
        tableLeave.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableLeave.setShowGrid(false);
        tableLeave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableLeaveMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tableLeave);

        comboBoxOptions.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        comboBoxOptions.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pending", "Approved", "Refused" }));
        comboBoxOptions.setBorder(javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        comboBoxOptions.setPreferredSize(new java.awt.Dimension(56, 29));
        comboBoxOptions.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxOptionsItemStateChanged(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Options");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 930, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(comboBoxOptions, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(comboBoxOptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void comboBoxOptionsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxOptionsItemStateChanged
    
        if (comboBoxOptions.getSelectedIndex() == 0) {
            loadPendingLeaveToTable();
        } else if (comboBoxOptions.getSelectedIndex() == 1) {
            loadApprovedLeaveToTable();
        } else if (comboBoxOptions.getSelectedIndex() == 2) {
            loadRefusedLeaveToTable();
        }
        
    }//GEN-LAST:event_comboBoxOptionsItemStateChanged

    private void tableLeaveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableLeaveMousePressed
        
        if (evt.getButton() != MouseEvent.BUTTON3)
            return;
        
        if (comboBoxOptions.getSelectedIndex() != 0)
            return;
        
        if (tableLeave.getSelectedRow() == -1)
            return;
        
        popupMenu.show(tableLeave, evt.getX(), evt.getY());
        
    }//GEN-LAST:event_tableLeaveMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboBoxOptions;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableLeave;
    // End of variables declaration//GEN-END:variables
}
