/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form;

import ComponentUtil.TableUtil;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Dictionary;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.basic.BasicListUI;
import javax.swing.plaf.basic.BasicListUI.ListSelectionHandler;
import javax.swing.table.DefaultTableModel;
import model.Candidate;
import model.Position;
import service.AlertService;
import service.CandidateService;

/**
 *
 * @author XXXXX
 */
public class InterviewForm extends javax.swing.JPanel {

    /**
     * Creates new form InterviewForm
     */
    private CandidateService candidateService;
    private AlertService alertService;
    private JFrame mainForm;
    private JPopupMenu popupMenuNotIntervieCandidate;
    private JPopupMenu popupMenuPendingCandidate;
    private JPopupMenu popupMenuUpgradeCandidate;
    private static final Font DEFAULT_FONT = new Font("Tohama", Font.PLAIN, 12);

    
    public InterviewForm(JFrame mainForm) {
        initComponents();
        reInit(mainForm);
    }
    
    private void reInit(JFrame mainForm) {
        this.candidateService = CandidateService.create();
        this.alertService = AlertService.create();
        
        this.mainForm = mainForm;
        // table
        TableUtil.setCellTextAlignment(tableCandidate, JLabel.CENTER);
        tableCandidate.setDefaultEditor(Object.class, null);
        comboBoxTableOptionItemStateChanged(null);
        
        // popup menu new candidate
        JMenuItem menuItemAddInterview = new JMenuItem("Add Interview");
        menuItemAddInterview.setFont(DEFAULT_FONT);
        menuItemAddInterview.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                menuItemAddInterviewMouseClicked(e);
            }
            
        });
        // popup menu pending candidate
        JMenuItem menuItemInterviewStatus = new JMenuItem("Set interview status");
        menuItemInterviewStatus.setFont(DEFAULT_FONT);
        menuItemInterviewStatus.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                menuItemSetInterviewStatusMouseClicked(e);
            }
            
        });
        // popup menu upgrade candidate
        JMenuItem menuItemUpgradeCandidate = new JMenuItem("Upgrade candidate");
        menuItemUpgradeCandidate.setFont(DEFAULT_FONT);
        menuItemUpgradeCandidate.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                menuItemUpgradeCandidateMouseClicked(e);
            }
            
        });
        //
        popupMenuPendingCandidate = new JPopupMenu();
        popupMenuPendingCandidate.add(menuItemInterviewStatus);
        //
        popupMenuNotIntervieCandidate = new JPopupMenu();
        popupMenuNotIntervieCandidate.add(menuItemAddInterview);
        //
        popupMenuUpgradeCandidate = new JPopupMenu();
        popupMenuUpgradeCandidate.add(menuItemUpgradeCandidate);
        
    }
    
    private void loadNewCandidateToTable(List<Dictionary<String, Object>> candidates) {
        
        Object[][] data = null;
        String[] columnNames = new String[] {
            "Candidate Id",
            "Position Id",
            "Candidate Name",
            "Email",
            "Address",
            "Position",
            "Status",
            "Expect Salary",
            "Date Of Birth"
        };
        DefaultTableModel tableModel = new DefaultTableModel(data, columnNames);
        tableCandidate.setModel(tableModel);
        TableUtil.hideColumns(new String[] { "Candidate Id", "Position Id" }, tableCandidate);

        for (Dictionary<String, Object> row : candidates) {
            tableModel.addRow(new Object[] {
                row.get("candidate_id"),
                row.get("apply_for"),
                row.get("candidate_name"),
                row.get("email"),
                row.get("address"),
                row.get("position"),
                row.get("status"),
                row.get("expect_salary"),
                row.get("dob")
            });
        }
    }
    
    private void loadPendingCandidateToTable(List<Dictionary<String, Object>> candidates) {
        Object[][] data = null;
        String[] columnNames = new String[] {
            "Candidate Id",
            "Position Id",
            "Candidate Name",
            "Email",
            "Address",
            "Position",
            "Status",
            "Expect Salary",
            "Date Of Birth",
            "Interview Type"
        };
        DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(data, columnNames);
        tableCandidate.setModel(tableModel);
        TableUtil.hideColumns(new String[] { "Candidate Id", "Position Id" }, tableCandidate);
        
        for (Dictionary<String, Object> row : candidates) {
            tableModel.addRow(new Object[] {
                row.get("candidate_id"),
                row.get("apply_for"),
                row.get("candidate_name"),
                row.get("email"),
                row.get("address"),
                row.get("position"),
                row.get("status"),
                row.get("expect_salary"),
                row.get("dob"),
                row.get("total_interviewed")
            });
        }
    }
    
    private void loadPassedCandidateToTable(List<Dictionary<String, Object>> candidates) {
        Object[][] data = null;
        String[] columnNames = new String[] {
            "Candidate Id",
            "Position Id",
            "Candidate Name",
            "Gender",
            "Email",
            "Address",
            "Position",
            "Status",
            "Expect Salary",
            "Date Of Birth",
            "Tel"
        };
        DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(data, columnNames);
        tableCandidate.setModel(tableModel);
        TableUtil.hideColumns(new String[] { "Candidate Id", "Position Id", "Tel" }, tableCandidate);
        
        for (Dictionary<String, Object> row : candidates) {
            tableModel.addRow(new Object[] {
                row.get("candidate_id"),
                row.get("apply_for"),
                row.get("candidate_name"),
                row.get("gender"),
                row.get("email"),
                row.get("address"),
                row.get("position"),
                row.get("status"),
                row.get("expect_salary"),
                row.get("dob"),
                row.get("tel")
            });
        }
    }
    
    private void loadFailedCandidateToTable(List<Dictionary<String, Object>> candidates) {
        Object[][] data = null;
        String[] columnNames = new String[] {
            "Candidate Id",
            "Position Id",
            "Candidate Name",
            "Email",
            "Address",
            "Position",
            "Status",
            "Expect Salary",
            "Date Of Birth"
        };
        DefaultTableModel tableModel = new javax.swing.table.DefaultTableModel(data, columnNames);
        tableCandidate.setModel(tableModel);
        TableUtil.hideColumns(new String[] { "Candidate Id", "Position Id" }, tableCandidate);
        
        for (Dictionary<String, Object> row : candidates) {
            tableModel.addRow(new Object[] {
                row.get("candidate_id"),
                row.get("apply_for"),
                row.get("candidate_name"),
                row.get("email"),
                row.get("address"),
                row.get("position"),
                row.get("status"),
                row.get("expect_salary"),
                row.get("dob")
            });
        }
    }
    
    private void refreshTableData() {
        comboBoxTableOptionItemStateChanged(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addCandidate = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCandidate = new javax.swing.JTable();
        comboBoxTableOption = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));

        addCandidate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        addCandidate.setText("New Candidate");
        addCandidate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCandidateActionPerformed(evt);
            }
        });

        tableCandidate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tableCandidate.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tableCandidate.setRowHeight(20);
        tableCandidate.setRowMargin(3);
        tableCandidate.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableCandidate.setShowGrid(false);
        tableCandidate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCandidateMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableCandidate);

        comboBoxTableOption.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        comboBoxTableOption.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Not Interviewed Candidate", "Pending Candidate", "Failed Candidate", "Passed Candidate" }));
        comboBoxTableOption.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxTableOptionItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 943, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comboBoxTableOption, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addCandidate)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addCandidate)
                    .addComponent(comboBoxTableOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addCandidateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCandidateActionPerformed
        // TODO add your handling code here:
        AddCandidateForm addCandidateForm = new AddCandidateForm(null, true);
        addCandidateForm.setLocationRelativeTo(mainForm);
        addCandidateForm.setVisible(true);
    }//GEN-LAST:event_addCandidateActionPerformed

    private void comboBoxTableOptionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxTableOptionItemStateChanged
        // TODO add your handling code here:
        int selectedIndex = comboBoxTableOption.getSelectedIndex();
        
        try {
            if (selectedIndex == 0) {
                loadNewCandidateToTable(candidateService.getNotInterviewedCandidate());
            } else if (selectedIndex == 1) {
                loadPendingCandidateToTable(candidateService.pendingInterviewCandidate());
            } else if (selectedIndex == 3) {
                loadPassedCandidateToTable(candidateService.getPassCandidate());
            } else if (selectedIndex == 2) {
                loadFailedCandidateToTable(candidateService.getFailedCandidate());
            }
        } catch (SQLException ex) {
            Logger.getLogger(InterviewForm.class.getName()).log(Level.SEVERE, null, ex);
            alertService.alertError(this);
        }
    }//GEN-LAST:event_comboBoxTableOptionItemStateChanged

    private void tableCandidateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableCandidateMouseClicked
        // TODO add your handling code here:
        if (evt.getButton() != MouseEvent.BUTTON3)
            return;
        int selectedIndex = tableCandidate.getSelectedRow();
        if (selectedIndex == -1)
            return;
        
        if (comboBoxTableOption.getSelectedIndex() == 0) {
            popupMenuNotIntervieCandidate.show(tableCandidate, evt.getX(), evt.getY());
        } else if (comboBoxTableOption.getSelectedIndex() == 1) {
            popupMenuPendingCandidate.show(tableCandidate, evt.getX(), evt.getY());
        } else if (comboBoxTableOption.getSelectedIndex() == 3) {
            popupMenuUpgradeCandidate.show(tableCandidate, evt.getX(), evt.getY());
        }
        
    }//GEN-LAST:event_tableCandidateMouseClicked
    
    
    private void menuItemAddInterviewMouseClicked(MouseEvent evt) {
        int selectedRow = tableCandidate.getSelectedRow();
        DefaultTableModel tableModel = (DefaultTableModel) tableCandidate.getModel();
        int candidateId = (int) tableModel.getValueAt(selectedRow, 0);
        String candidateName = (String) tableModel.getValueAt(selectedRow, 2);
        int positionId = (int) tableModel.getValueAt(selectedRow, 1);
        String positionName = (String) tableModel.getValueAt(selectedRow, 5);
        
        Candidate candidate = new Candidate();
        candidate.setCandidateId(candidateId);
        candidate.setCandidateName(candidateName);
        
        Position position = new Position();
        position.setPositionId(positionId);
        position.setPosition(positionName);
        
        AddInterviewDetailForm addInterviewDetailForm = new AddInterviewDetailForm(mainForm, true);
        
        addInterviewDetailForm.setCandidate(candidate);
        addInterviewDetailForm.setPosition(position);
        addInterviewDetailForm.setVisible(true);
        
        refreshTableData();
    }
    
    private void menuItemSetInterviewStatusMouseClicked(MouseEvent evt) {
        int selectedRow = tableCandidate.getSelectedRow();
        DefaultTableModel tableModel = (DefaultTableModel) tableCandidate.getModel();
        
        int candidateId = (int) tableModel.getValueAt(selectedRow, 0);
        String candidateName = (String) tableModel.getValueAt(selectedRow, 2);
        String position = (String) tableModel.getValueAt(selectedRow, 5);
        int positionId = (int) tableModel.getValueAt(selectedRow, 1);
        int interviewType = (int) tableModel.getValueAt(selectedRow, 9);
        
        Candidate candidate = new Candidate();
        candidate.setCandidateId(candidateId);
        candidate.setCandidateName(candidateName);
        
        Position pos = new Position();
        pos.setPosition(position);
        pos.setPositionId(positionId);
        
        AdjustInterviewStatusForm adjustInterviewStatusForm = new AdjustInterviewStatusForm(mainForm, true);
        adjustInterviewStatusForm.setCandidate(candidate);
        adjustInterviewStatusForm.setPosition(pos);
        adjustInterviewStatusForm.setInterviewType(interviewType);
        
        adjustInterviewStatusForm.setVisible(true);
        
        refreshTableData();
    }
    
    private void menuItemUpgradeCandidateMouseClicked(MouseEvent evt) {
        int selectedRow = tableCandidate.getSelectedRow();
        DefaultTableModel tableModel = (DefaultTableModel) tableCandidate.getModel();
        
        int candidateId = (int) tableModel.getValueAt(selectedRow, 0);
        String candidateName = (String) tableModel.getValueAt(selectedRow, 2);
        String gender = (String) tableModel.getValueAt(selectedRow, 3);
        String email = (String) tableModel.getValueAt(selectedRow, 4);
        String address = (String) tableModel.getValueAt(selectedRow, 5);
        String status = (String) tableModel.getValueAt(selectedRow, 7);
        double expectSalary = (Double) tableModel.getValueAt(selectedRow, 8);
        Timestamp dob = (Timestamp) tableModel.getValueAt(selectedRow, 9);
        String tel = (String) tableModel.getValueAt(selectedRow, 10);
        
        int positionId = (int) tableModel.getValueAt(selectedRow, 1);
        String positionName = (String) tableModel.getValueAt(selectedRow, 6);
        
        Candidate candidate = new Candidate();
        candidate.setCandidateId(candidateId);
        candidate.setCandidateName(candidateName);
        candidate.setEmail(email);
        candidate.setTel(tel);
        candidate.setAddress(address);
        candidate.setStatus(status);
        candidate.setExpectSalary(expectSalary);
        candidate.setDob(dob);
        candidate.setGender(gender);
        
        Position position = new Position();
        position.setPositionId(positionId);
        position.setPosition(positionName);
        
        UpgradeCandidateForm upgradeCandidateForm = new UpgradeCandidateForm(mainForm, true);
        upgradeCandidateForm.setCandidate(candidate);
        upgradeCandidateForm.setPosition(position);
        upgradeCandidateForm.setVisible(true);
        
        refreshTableData();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addCandidate;
    private javax.swing.JComboBox<String> comboBoxTableOption;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableCandidate;
    // End of variables declaration//GEN-END:variables

}
