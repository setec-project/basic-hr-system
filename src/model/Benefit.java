/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author XXXXX
 */
public class Benefit {
    private int benefitId;
    private String benefitName;
    private double payrate;
    
    public Benefit() { }

    public Benefit(String benefitName, double payrate) {
        this.benefitName = benefitName;
        this.payrate = payrate;
    }

    public int getBenefitId() {
        return benefitId;
    }

    public void setBenefitId(int benefitId) {
        this.benefitId = benefitId;
    }

    public String getBenefitName() {
        return benefitName;
    }

    public void setBenefitName(String benefitName) {
        this.benefitName = benefitName;
    }

    public double getPayrate() {
        return payrate;
    }

    public void setPayrate(double payrate) {
        this.payrate = payrate;
    }

    @Override
    public String toString() {
        return getBenefitName();
    }
    
    
    
    
}
