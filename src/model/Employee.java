/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ComponentUtil.TimeFormatterUtil;
import java.sql.Timestamp;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author XXXXX
 */
public class Employee {
    
    private int employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String status;
    private Timestamp dob;
    private String gender;
    private int positionId;
    private String bankAccountNumber;
    private double baseSalary;
    private double otInOneHour;
    private Integer bankId;
    private Integer departmentId;
    
    public Employee() { }

    public Employee(String firstName, String lastName, String email, String address, String status, Timestamp dob, String gender, int position_id, String bankAccountNumber, double baseSalary, double otInOneHour, int bankId, int departmentId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.status = status;
        this.dob = dob;
        this.gender = gender;
        this.positionId = position_id;
        this.bankAccountNumber = bankAccountNumber;
        this.baseSalary = baseSalary;
        this.otInOneHour = otInOneHour;
        this.bankId = bankId;
        this.departmentId = departmentId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return StringUtils.capitalize(firstName);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return StringUtils.capitalize(lastName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public java.sql.Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public double getOtInOneHour() {
        return otInOneHour;
    }

    public void setOtInOneHour(double otInOneHour) {
        this.otInOneHour = otInOneHour;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return getLastName() + " " + getFirstName();
    }
    
}
