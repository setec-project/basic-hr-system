/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author XXXXX
 */
public class Position {
    
    private int positionId;
    private String position;

    public Position() { }
    
    public Position(String position) {
        this.position = position;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getPosition() {
        return StringUtils.capitalize(position);
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return getPosition();
    }
    
    
    
    
}
