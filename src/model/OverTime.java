/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author XXXXX
 */
public class OverTime {
    
    private Integer overTimeId;
    private Integer employeeId;
    private Timestamp otDate;
    private String startTime;
    private String endTime;
    private Double hour;
    private String description;
    
    public OverTime() { }

    public Integer getOverTimeId() {
        return overTimeId;
    }

    public void setOverTimeId(Integer overTimeId) {
        this.overTimeId = overTimeId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Timestamp getOtDate() {
        return otDate;
    }

    public void setOtDate(Timestamp otDate) {
        this.otDate = otDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Double getHour() {
        return hour;
    }

    public void setHour(Double hour) {
        this.hour = hour;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    
}
