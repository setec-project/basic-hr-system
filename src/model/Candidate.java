/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ComponentUtil.TimeFormatterUtil;
import java.sql.Timestamp;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author XXXXX
 */
public class Candidate {
    private int candidateId;
    private String candidateName;
    private String email;
    private String tel;
    private String address;
    private String status;
    private Timestamp dob;
    private String gender;
    private int positionId;
    private int numberOfInterview;
    private double expectSalary;
    
    public Candidate() { };

    public Candidate(String candidateName, String email, String tel, String address, String status, Timestamp dob, String gender, int position, int numberOfInterview, double expectSalary) {
        this.candidateName = candidateName;
        this.email = email;
        this.tel = tel;
        this.address = address;
        this.status = status;
        this.dob = dob;
        this.gender = gender;
        this.positionId = position;
        this.numberOfInterview = numberOfInterview;
        this.expectSalary = expectSalary;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getCandidateName() {
        return StringUtils.capitalize(candidateName);
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return StringUtils.capitalize(status);
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getGender() {
        return StringUtils.capitalize(gender);
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getPosition() {
        return positionId;
    }

    public void setPosition(int position) {
        this.positionId = position;
    }

    public int getNumberOfInterview() {
        return numberOfInterview;
    }

    public void setNumberOfInterview(int numberOfInterview) {
        this.numberOfInterview = numberOfInterview;
    }

    public double getExpectSalary() {
        return expectSalary;
    }

    public void setExpectSalary(double expectSalary) {
        this.expectSalary = expectSalary;
    }
    
    
    
    
}
