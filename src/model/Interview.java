/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Time;
import java.sql.Timestamp;

/**
 *
 * @author XXXXX
 */
public class Interview {
    
    private int interviewId;
    private Timestamp interviewDate;
    private int interviewType;
    private int isPassed;
    private int candidateId;
    private String startTime;
    private String endTime;
    
    public Interview() { }

    public Interview(Timestamp interviewDate, int interviewType, int isPassed, int candidateId, String startTime, String endTime) {
        this.interviewDate = interviewDate;
        this.interviewType = interviewType;
        this.isPassed = isPassed;
        this.candidateId = candidateId;
        this.startTime = startTime;
        this.endTime = endTime;
    }
    
    

    public int getInterviewId() {
        return interviewId;
    }

    public void setInterviewId(int interviewId) {
        this.interviewId = interviewId;
    }

    public Timestamp getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(Timestamp interviewDate) {
        this.interviewDate = interviewDate;
    }

    public int getInterviewType() {
        return interviewType;
    }

    public void setInterviewType(int interviewType) {
        this.interviewType = interviewType;
    }

    public int getIsPass() {
        return isPassed;
    }

    public void setIsPassed(int isPassed) {
        this.isPassed = isPassed;
    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    
    
    
    
}
